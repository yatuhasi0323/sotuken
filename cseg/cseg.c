#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "cseg.h"


int main(){
    int i, j, l, k;
    
    matrix pl;//パスロス
    rayleigh  h;
    double thetap[FN];
    double ss = 0.0;//フェージング係数
    
    //シャドウイングの変数
    matrix s;//シャドウイング係数
    double cn = 0.0;//係数
    double a = 2.0/WN;
    double fx[WN/2];
    double fy[WN/2];
    double fu[WN/2];
    double fv[WN/2];
    
    frequency fn;//Fn
    
    double d, d0;

    double beta = 0.0;//確率変数
    double phi = 0.0;
    double thetas[WN];
    
    point Tx;//ノード座標
    matrix dis;//距離
    
    chanel ch[N][CN];
    
    int rout[N];//ルート配列
    
    double pmin;

    
    init_genrand((unsigned)time(NULL));

    
    //初期条件
    for(i=0;i<N;i++){
        Tx.x[i] = 15.0 * i;//x座標
        Tx.y[i] = 0.0;//y座標
        Tx.id[i] = i + 1;//ノード番号
        Tx.ch[i] = 0;//使用チャネル　使ってない：０
        rout[i] = Tx.id[i];
    }
    
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            pl.square[i][j] = 0.0;
        }
    }
    
    distance(&dis, &Tx);
    
    //シャドウイングの定数ーーーーーーーーーーーー
    cn = sqrt(a);
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
        //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
        // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
    for(i=0;i<WN/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(WN/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(WN/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(WN/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(WN/2)] = fy[i];
    }
    
    for(i = 0;i<WN/2;i++){
        thetas[i] = 2 * M_PI * genrand_res53();
        thetas[i+WN/2] = thetas[i];
    }
    //--------------------------
    
    //シャドウイング算出
    //shadowing(&s, &Tx, thetas, &fn, cn);
    for(i = 0;i<N;i++){
        for(j = 0;j<N;j++){
            //シャドウイング係数の算出
            
            for(k = 0;k<WN;k++){
                s.square[i][j] += cn * cos( 2 * M_PI * (fn.x[k] * Tx.x[i] + fn.y[k] * Tx.y[i] + fn.u[k] * Tx.x[j] + fn.v[k] * Tx.y[j] ) + thetas[k]);
            }
            
            // printf("%8lf\t",ans->square[i+L][j+L]); //データ表示
        }
        //sprintf("\n");
    }

    
    //レイリーフェージング算出
    for(l = 0; l < FN; l++){
        thetap[l]= genrand_real2() * 2 * M_PI;
    }
    //初期化
    h.real = 0.0;
    h.image = 0.0;
    
    for (l = 0; l < FN; l++) {
        h.real += cos(thetap[l]);
        h.image += sin(thetap[l]);
    }
    
    h.real /= sqrt(FN);
    h.image /= sqrt(FN);
    
    ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
    ss = 10 * log10(ss * ss);//フェージング係数
    
    //パスロス
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            d = dis.square[i][j];
            if(d<=D){
                d0=d;
            }else{
                d0=D;
            }
            
            pl.square[i][j] = Ptx + 20*log10(C/F/(4.0*M_PI*d0))-10*n*log10(d/d0);
            //pl.square[i][j] = pow(10.0, (pl.square[i][j] / 10.0));//W変換
            
             //printf("%f\t",pl.square[i][j]);
            
        }
         //printf("\n");
    }
    
    double df[N][N];
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            df[i][j] = pl.square[i][j] + ss + s.square[i][j];
            //pl.square[i][j] = df[i][j];//パスロス
            pl.square[i][j] = pow(10.0, (df[i][j] / 10.0));//パスロス
        }
    }
    
    
    
    //パスロス表示ーーーーーーーーーーーーーー

    for(i=0;i<N;i++){
     for(j=0;j<N;j++){
     printf("%16.10lf\t", pl.square[i][j]);
     }
     printf("\n");
}
    printf("\n\n");

    
    //チャネル住み分け

    Tx.ch[ rout[0] - 1 ] = 1;//最初 ch1を使用
    
    double df2[N][N];

    for(i = 1;i<N;i++){//ノード選択ループ>ルートから持ってくる
        for(j = 0;j<3;j++){//チャネル選択ループ
            for(k = 0;k<N;k++){//同一チャネル選別ループ
                if(Tx.ch[k] == j+1 && i != k){//チャネル使用ノード検索 どうチャネルである場合
                    
                     df2[i][j] += pl.square[rout[i] - 1][k];//チャネル消費電力の総和
                }else{
                    df2[i][j] += 0.0;
                }
                ch[i][j].cpw = df2[i][j];
                
            }
            
            /*
            if(ch[i][j].cpw < Pminw){//チャネルの使用電力が閾値以下であった場合
                Tx.ch[i] = j + 1;//チャネル決定
            }
             */
        }
        
        if(Tx.ch[i] == 0){
            pmin = ch[i][0].cpw;
            for(j = 1;j<3;j++){
                if(pmin > ch[i][j].cpw){
                    Tx.ch[i] = j + 1;
                }else{
                    Tx.ch[i] = 1;
                }
            }
        }
        
        
    }
    
   // pl.square[rout[i] - 1][k] > Pminw
   // if(Tx.ch[i] == Tx.ch[j] && i != j){
    //    ch[j].cpw += pl.square[i][j];
   // }
    for(i = 0;i<N;i++){
    printf("%d\t%d\n", Tx.ch[i], rout[i]);
    }
    
    
    for(i = 0;i<N;i++){
        for(j = 0;j<3;j++){
    printf("%16.15lf\t", ch[i][j].cpw);
        }
printf("\n");
}
    
    
    
    
    
    
    
    
}

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "parame.h"
#include "cseg.h"


void shadowing(matrix *ans, point *ap, double *theta, frequency *f, double c){
    int i = 0, j = 0, k = 0;
    
    for(i = 0;i<N;i++){
        for(j = 0;j<N;j++){
            //シャドウイング係数の算出
            
            for(k = 0;k<WN;k++){
                ans->square[i][j] += c * cos( 2 * M_PI * (f->x[k] * ap->x[i] + f->y[k] * ap->y[i] + f->u[k] * ap->x[j] + f->v[k] * ap->y[j] ) + theta[k]);
            }
            
           // printf("%8lf\t",ans->square[i+L][j+L]); //データ表示
        }
        //sprintf("\n");
    }
}

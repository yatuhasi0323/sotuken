#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "main.h"

//乱数作るやつ
int main(){
    int i=0, j = 0, k = 0;
    
    point tx, rx;
    
    double beta = 0.0;//確率変数
    double phi = 0.0;
    double theta[N];
    
    double fx[N/2];
    double fy[N/2];
    double fu[N/2];
    double fv[N/2];

    double a = 2.0/N;
    double cn = 0.0;//係数
    double av = 0.0;//平均
    double ds = 0.0;//分散
    
    double c = 0.0;//count
    double sum = 0.0;//sum
    
    double nd[X];//正規分布
    
    matrix s;//シャドウイング係数
    
    
    frequency fn;//Fn


        init_genrand((unsigned)time(NULL));
    
    //cn
    
    cn = sqrt(a);
    //printf("%lf\n", cn);
    
    
   // printf("alpha = %lf\n", Alpha);
    //fx,fyの決定
    for(i=0;i<N/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
      //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
    //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
   // printf("\n----------------\n\n");
        //fu,fvの決定
    for(i=0;i<N/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
      //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
       // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
   // printf("\n----------------\n\n");
    
    //fn に 各fを代入
    
    for(i=0;i<N/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(N/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(N/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(N/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(N/2)] = fy[i];
    }
    
    
    //for(i=0;i<N;i++){
    //    printf("%2d | %10lf | %10lf | %10lf | %10lf\n",i+1, fn.x[i], fn.y[i], fn.u[i], fn.v[i]);
    //}
    
    
    //シータnに乱数代入
    
    for(i = 0;i<N/2;i++){
    theta[i] = 2 * M_PI * genrand_res53();
    theta[i+N/2] = theta[i];
    }
    
/*
for(i = 0;i<N;i++){
   printf("%lf\n\n",theta[i]);
}
  //  printf("------------");
 */
    
    //座標
    
    tx.x = 0.0;
    tx.y = 0.0;
    
    //rx.x = 0.0;
    //rx.y = 0.0;

   /*
    for(i = 0;i<=2*L;i++){
        printf("%8d\t",i);// １行目の表示
    }
    printf("\n");
    */
     
    
    for(i = -L;i<=L;i++){
        for(j = -L;j<=L;j++){
            rx.x = (double)i;
            rx.y = (double)j;
            
            //tx.x = i;
            //rx.y = j;
    //シャドウイング係数の算出
            
            for(k = 0;k<N;k++){
                s.square[i+L][j+L] += cn * cos( 2 * M_PI * (fn.x[k] * tx.x + fn.y[k] * tx.y + fn.u[k] * rx.x + fn.v[k] * rx.y ) + theta[k]);
            }
                
            printf("%8lf\t",s.square[i+L][j+L]); //データ表示
    }
        printf("\n");
    }

    
    for(i = 0;i <= 2*L;i++){
        for(j = 0;j <= 2*L;j++){
            
            sum += s.square[i][j];
            c += 1.0;
        }
    }
    
    av = sum / c;
    
    // printf("%8lf\n", av);
    
    ds = dispersion(av, &s);
    
    // printf("%8lf\n", ds);

    ndist(av, ds, nd);

        
}

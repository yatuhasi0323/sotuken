#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "parame.h"
#include "main.h"


double  average(matrix *sp){
    int i, j;
    double s=0.0;
    double c = 0.0;
    double ans = 0.0;
    
    for(i = 0;i <= 2*L;i++){
        for(j = 0;j <= 2*L;j++){
            
            s += sp->square[i][j];
            c += 1.0;
        }
}
    
  //  printf("平均の割る前 %lf\n", s);
    ans = s / c;
   
    return ans;
}

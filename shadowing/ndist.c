#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "parame.h"
#include "main.h"


void ndist(double a, double ad, double *ans){
    
    int i;
    double x;
    
    for(i = -(X-1)/2;i <= (X-1)/2;i++){
        x = i*0.1;
        ans[i+(X-1)/2] = (1/(sqrt(2*M_PI)*sqrt(ad)))*pow(M_E, -(((x - a)*(x - a))/(2*ad)));
    //    printf("%lf\t%lf\n", x, ans[i+(X-1)/2]);
    }
    
}

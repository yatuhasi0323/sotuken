/*
#ifndef __semi1
#define __semi1
*/

//送信機の構造体
typedef struct {
    double x[N];
    double y[N];
    double d[N];
}point;

//２次元配列の構造体
typedef struct {
    double square[N][N];
}matrix;


void distance(matrix *dp,point *xp,point *yp);
void passloss(matrix *sp, matrix *dp);
void snr(double *g, matrix *pp);
void srx(double *a, double *b);
double average(double *a, double b);
double dispersion(double *a, double b, double ave);




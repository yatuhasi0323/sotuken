#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "semi1.h"

//乱数作るやつ
int main(){
    int i=0, j=0;
    point tx;
    point rx;
    matrix dis;//distance
    matrix p;//power
    double g[N];//snr
    double c[N];//srx
    double ca[M];//average
    double disp[M];//dispersin

    double av=0;
    
    init_genrand((unsigned)time(NULL));


    for(j=0;j<M;j++){//メインループ
    
    //配置部
    
    for(i=0;i<N;i++){
        tx.x[i] = L * genrand_res53()-0.5;
        tx.y[i] = L * genrand_res53()-0.5;
        tx.d[i] = 2*M_PI * genrand_res53();
        
  //      printf("Tx[%d]:x=%f y=%f d=%f\n",i+1, tx.x[i], tx.y[i], tx.d[i]);
        rx.x[i] = R * cos(tx.d[i]) + tx.x[i];
        rx.y[i] = R * sin(tx.d[i]) + tx.y[i];
        rx.d[i] = 0.0;

    //    printf("Rx[%d]:x=%f y=%f d=%f\n",i+1, rx.x[i], rx.y[i], rx.d[i]);
    }
    
  //  printf("\n----------------------------------------------------------------------------\n");

    //送信機に対する全受信機の距離

    distance(&dis, &tx, &rx);
    
   // printf("\n----------------------------------------------------------------------------\n");
    
    //パスロス

    passloss(&p, &dis);
    
    //snr
    
  //  printf("\n----------------------------------------------------------------------------\n");
     
    snr(g, &p);
    
 //   printf("\n----------------------------------------------------------------------------\n");
    
    srx(c, g);
        
        ca[j] = average(c, N);
        disp[j] = dispersion(c, N, ca[j]);

    }
    
 //   printf("\n----------------------------------------------------------------------------\n");
/*
        printf("平均値\t\t分散値\n");
    for(j=0;j<M;j++){
    printf("%f\t%f\n", ca[j], disp[j]);
    }
    */
    av = average(ca, M);

    printf("%lf\n", av);
}



//各パラメータ

//ループ回数
#define M 1
//シミュレーションエリア
#define L 150
//中継のーど数
#define N 100
//送信機と受信機の距離

//平均雑音電力
#define N0 -90

//搬送波周波数
#define F 2.4e9
//送信電力(dBm)
#define Ptx  17
//伝搬係数
#define n 3.5
//do
#define D  1.0

//de-correlation distance (5m:都市 20m:田舎)
#define Dcor 20
//光速
#define  C 2.98e8

//最低受信可能電力Pmin [dBm]
#define Pmin -75

//波数
#define WN 500

//素波数
#define FN 64

//アルファ
#define Alpha log(2)/Dcor

//係数
#define ALPHA 1/0.895

//ホップ上限
#define  MR 15


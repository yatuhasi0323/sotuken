#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "aodv.h"

//乱数作るやつ
int main(){
    int i=0, j=0, l=0;
    
    
    point Tx;//端末座標およびアドレス
    matrix dis;//端末間距離
    
    int sid1 = 1;//sorce1のid
    int did1 = N;//destination1のid
    
    int sid2 = 2;//sorce2のid
    int did2 = N-1;//destination2のid
    
   // int count = 0;//カウンタ
    
    
    matrix pl;//パスロス
    rayleigh  h;
    double thetap[FN];
    double ss = 0.0;//フェージング係数
    
    //シャドウイングの変数
    matrix s;//シャドウイング係数
    double cn = 0.0;//係数
    double a = 2.0/WN;
    double fx[WN/2];
    double fy[WN/2];
    double fu[WN/2];
    double fv[WN/2];
    
    frequency fn;//Fn
    
    double beta = 0.0;//確率変数
    double phi = 0.0;
    double thetas[WN];

    hedder rreq[N][2];
    
    int rout[MR][2];
    
    // double p[N];
  //  double g[N];
    
    
    init_genrand((unsigned)time(NULL));
    
//座標割当
    
    //Sorceの座標
    Tx.x[0] = -L/2;
    Tx.y[0] = L/2;
    Tx.id[0] = 1;
    
    Tx.x[1] = -L/2;
    Tx.y[1] = -L/2;
    Tx.id[1] = 2;
    
    //Destinationの座標
    Tx.x[N-1] = L/2;
    Tx.y[N-1] = -L/2;
    Tx.id[N-1] = N;
    
    Tx.x[N-2] = L/2;
    Tx.y[N-2] = L/2;
    Tx.id[N-2] = N-1;

    
    //relyの座標
    for(i = 2;i<N-2;i++){
        Tx.x[i] = L * (genrand_res53()-0.5);
        Tx.y[i] = L * (genrand_res53()-0.5);
        Tx.id[0] = i+1;
    }
    
    
    //端末間距離
    distance(&dis, &Tx);
    
    
    
    //初期化
    for(i=0;i<N;i++){
        for(j = 0;j < 2;j++){
            rreq[i][j].rid = 0;
            rreq[i][j].pid = 0;
            rreq[i][j].hop = 0;
            rreq[i][j].rflag = 0;
            rreq[i][j].tflag = 0;
        }
        }
        

    //シャドウイングの定数ーーーーーーーーーーーー
    cn = sqrt(a);
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
        //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
        // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
    for(i=0;i<WN/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(WN/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(WN/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(WN/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(WN/2)] = fy[i];
    }
    
    for(i = 0;i<WN/2;i++){
        thetas[i] = 2 * M_PI * genrand_res53();
        thetas[i+WN/2] = thetas[i];
    }
    //--------------------------
    
    //シャドウイング算出
    shadowing(&s, &Tx, thetas, &fn, cn);
    
    //レイリーフェージング算出
        for(l = 0; l < FN; l++){
            thetap[l]= genrand_real2() * 2 * M_PI;
        }
        //初期化
        h.real = 0.0;
        h.image = 0.0;
        
        for (l = 0; l < FN; l++) {
            h.real += cos(thetap[l]);
            h.image += sin(thetap[l]);
        }
        
        h.real /= sqrt(FN);
        h.image /= sqrt(FN);
        
        ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
        ss = 10 * log10(ss * ss);//フェージング係数
        
        passloss(&pl, &dis);

        
        for(i=0;i<N;i++){
            for(j=0;j<N;j++){
        pl.square[i][j] = pl.square[i][j] + ss + s.square[i][j];//パスロス
            }
        }
    
    
    //パスロス表示ーーーーーーーーーーーーーー
    /*
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            printf("%8lf\t", pl.square[i][j]);
        }
            printf("\n");
    }
    */
    
    
    //ルーティング
  
    for(j=0;j<N;j++){
        printf("%d\t%lf\t%lf\t\n",j+1, Tx.x[j], Tx.y[j]);
    }
    
    //1hop目
        for(j=0;j<N;j++){
            if(pl.square[sid1-1][j] > Pmin){//sorceと他との通信可能判定
                rreq[j][0].rid = sid1;
                rreq[j][0].pid = did1;
                rreq[j][0].hop +=1;
                rreq[j][0].rflag = 1;//sourceから受信しましたフラグ
        }
        }
    
    
//2hop以降
//
    while(rreq[N-1][0].pid != did1){//Dにパスが届くまでループ


    for(i = 1;i<N;i++){
    if(rreq[i][0].rflag == 1 && rreq[i][0].tflag == 0){//受信したか、すでに送信しているかの判定
        rreq[i][0].tflag = 1;//送信フラグ
    }
    }
    
    
    for(i = 1;i<N;i++){
        for(j = 1;j<N;j++){

        if(pl.square[i][j] > Pmin && rreq[i][0].tflag == 1 && rreq[j][0].tflag == 0){//受信可能かどうか
            if(rreq[j][0].rflag == 1){//受信側がすでに受信したことがあるかどうか(hop数が同じ場合のみ)
                if(rreq[j][0].p < pl.square[i][j]){
                    rreq[j][0].rid = i+1;
                    rreq[j][0].pid = did1;
                    rreq[j][0].hop = rreq[i][0].hop+1;
                    rreq[j][0].rflag = 1;
                    rreq[j][0].p = pl.square[i][j];
                }
            }else{
                
                rreq[j][0].rid = i+1;
                rreq[j][0].pid = did1;
                rreq[j][0].hop = rreq[i][0].hop+1;
                rreq[j][0].rflag = 1;
                rreq[j][0].p = pl.square[i][j];
            }
            
            }
        }
    }
    }
    
   // printf("%d\t\n", count);
    /*
            for(j=0;j<N;j++){
                printf("%d\t%d\t%d\t%d\t%d\t%lf\t%lf\t\n",j+1, rreq[j][0].rid, rreq[j][0].pid, rreq[j][0].hop, rreq[j][0].rflag, Tx.x[j], Tx.y[j]);
            }
    */
    
    //経路決定
    for(j = 0;j<MR;j++){
        rout[j][0] = 0;
    }
   
    rout[rreq[N-1][0].hop][0] = did1;
    
    for(i = rreq[N-1][0].hop; i > 0 ; i--){
        j = rout[i][0];
        //printf("%d\n",j);
        rout[i-1][0] = rreq[j-1][0].rid;
    }
    printf("\n");

    for(i = 0;i<MR;i++){
    printf("%d\n",rout[i][0]);
    }
    
    
    // ２つめのルート
    //1hop目
    for(j=0;j<N;j++){
        if(pl.square[sid2-1][j] > Pmin){//sorceと他との通信可能判定
            rreq[j][1].rid = sid2;
            rreq[j][1].pid = did2;
            rreq[j][1].hop +=1;
            rreq[j][1].rflag = 1;//sourceから受信しましたフラグ
        }
    }
    
    
    //2hop以降
    //
    while(rreq[N-2][1].pid != did2){//Dにパスが届くまでループ
        
        
        for(i = 1;i<N;i++){
            if(rreq[i][1].rflag == 1 && rreq[i][1].tflag == 0){//受信したか、すでに送信しているかの判定
                rreq[i][1].tflag = 1;//送信フラグ
            }
        }
        
        
        for(i = 1;i<N;i++){
            for(j = 1;j<N;j++){
                
                if(pl.square[i][j] > Pmin && rreq[i][1].tflag == 1 && rreq[j][1].tflag == 0){//受信可能かどうか
                    if(rreq[j][1].rflag == 1){//受信側がすでに受信したことがあるかどうか(hop数が同じ場合のみ)
                        if(rreq[j][1].p < pl.square[i][j]){
                            rreq[j][1].rid = i+1;
                            rreq[j][1].pid = did2;
                            rreq[j][1].hop = rreq[i][1].hop+1;
                            rreq[j][1].rflag = 1;
                            rreq[j][1].p = pl.square[i][j];
                        }
                    }else{
                        
                        rreq[j][1].rid = i+1;
                        rreq[j][1].pid = did2;
                        rreq[j][1].hop = rreq[i][1].hop+1;
                        rreq[j][1].rflag = 1;
                        rreq[j][1].p = pl.square[i][j];
                    }
                    
                }
            }
        }
    }
    
  //  printf("%d\t\n", count);
    /*
    for(j=0;j<N;j++){
        printf("%d\t%d\t%d\t%d\t%d\t%lf\t%lf\t\n",j+1, rreq[j][1].rid, rreq[j][1].pid, rreq[j][1].hop, rreq[j][1].rflag, Tx.x[j], Tx.y[j]);
    }
    */
    
    //経路決定
    for(j = 0;j<MR;j++){
        rout[j][1] = 0;
    }
    
    rout[rreq[N-2][1].hop][1] = did2;
    
    for(i = rreq[N-2][1].hop; i > 0 ; i--){
        j = rout[i][1];
        //printf("%d\n",j);
        rout[i-1][1] = rreq[j-1][1].rid;
    }
    printf("\n");
    
    for(i = 0;i<MR;i++){
        printf("%d\n",rout[i][1]);
    }
    

    
    
    
    
        
}

    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    

    
        
        //  plt[i][k] = pl[i][k];
        //printf("%d-%d;%8lf\t", i + 1, k + 1, pl[i][k]);//パスロス
    
    //printf("\n");
    /*else{
  for(k = 0; k < M; k++){//送信先
  pl[i][k] = 0.0;
  }
  }*/
//printf("%d;%d\t%8lf\t\n", i+1, b.flag[i], b.time[i]);


    



   
        
        //座標表示ーーーーーーーーーー
        /*
        for(k = 0; k<N;k++){
            printf("%d, x=%lf, y = %lf\n", k, Tx.x[k], Tx.y[k]);
        }
         */
        
        

        
        
     //





#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "main.h"

int main(){
    int i, j, k, l, m;
    double t = 0.0;//時間
    double pd1[M][N];//1基準の位相差
    //double pd4[M][N];//4基準の位相差

    point ap;//アクセスポインと
    
    beacon b;//ビーコン
    
    double o[M];//振動子の瞬時角速度
    double theta[M];//振動子の位相(リセットかけてる方)
    double total[M];//振動子の位相
    double r[M][N];//振動子の位相配列
    
    
    double d = 0.0;//端末間距離
    double pl[M][M];//パスロス
    double d0  = 0.0;
    rayleigh  h;
    double thetap[FN];
    double ss = 0.0;//フェージング係数
    
    //シャドウイングの変数
    double s[M][M];//シャドウイング係数
    double cn = 0.0;//係数
    double a = 2.0/WN;
    double fx[WN/2];
    double fy[WN/2];
    double fu[WN/2];
    double fv[WN/2];
    
    frequency fn;//Fn
    
    double beta = 0.0;//確率変数
    double phi = 0.0;
    double thetas[WN];

    
    double pk[M][M];//パケット受信回数
    double tb[M];//ビーコン発信回数

    
    init_genrand((unsigned)time(NULL));
    
    //シャドウイングの値
    cn = sqrt(a);
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
        //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
    for(i=0;i<N/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
        // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
    for(i=0;i<N/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(N/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(N/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(N/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(N/2)] = fy[i];
    }
    
    for(i = 0;i<WN/2;i++){
        thetas[i] = 2 * M_PI * genrand_res53();
        thetas[i+WN/2] = thetas[i];
    }
    

    

    
    printf("初期位相\t");
  //初期化
    for(i = 0; i < M; i++){
        o[i] = Omega;//初期加速度
        
        theta[i] = 2*M_PI*genrand_real3();//初期位相
       // theta[i] = 0.0;
        total[i] = theta[i];
          printf("%8lf\t", theta[i]);
        
        b.flag[i] = 0;
        b.time[i] = 0.0;
    }
    
    printf("\n");

    //apの座標割当 (直列配置)
    
    for(i = 0; i < M;i++){
    ap.x[i] = 0.0 + D * (double)i;
    ap.y[i] = 0.0;
    }
    
    for(i = 0;i<M;i++){
        for(j = 0;j< M;j++){
            //シャドウイング係数の算出
            
            for(k = 0;k<WN;k++){
                s[i][j] += cn * cos( 2 * M_PI * (fn.x[k] * ap.x[i] + fn.y[k] * ap.y[i] + fn.u[k] * ap.x[j] + fn.v[k] * ap.y[j] ) + thetas[k]);
            }
            
            //printf("%8lf\t",s.square[i+L][j+L]); //データ表示
        }
        //printf("\n");
    }


        //init(pd);
        
    for(m = 0; m < ROOP; m++){
        
        for(j = 0;j<N;j++){
            for(i = 0; i < M; i++){
                pd1[i][j] = 0.0;
                //pd4[i][j] = 0.0;
            }
        }
        
    for(j = 0;j<N;j++){//メインループ
        t = ( (double)j + (double)m * N ) * Dt;//サンプリングタイム
        
         //振動子の位相計算ーーーーーーーーーーーーーー
        for(i = 0; i < M; i++){
             if(j != 0){
             theta[i] = theta[i] + o[i] * Dt;
             total[i] = total[i] + o[i] * Dt;
             }
             r[i][j] = theta[i];
         //位相差計算ーーーーーーーー
             pd1[i][j] = total[0] - total[i];
         

            

            //振動子のフラグ生成
            if(theta[i] >= 2*M_PI){
                theta[i] = theta[i] - 2 * M_PI;
                o[i] = Omega;
                b.flag[i] = 1;
                b.time[i] = t;
                tb[i] += 1.0;
                
                //パスロス計算
                for(k = 0; k < M; k++){//送信先
                    
                    //レイリーフェージング
                    for(l = 0; l < FN; l++){
                        thetap[l]= genrand_real2() * 2 * M_PI;
                    }
                    //初期化
                    h.real = 0.0;
                    h.image = 0.0;
                    
                    for (l = 0; l < FN; l++) {
                        h.real += cos(thetap[l]);
                        h.image += sin(thetap[l]);
                    }
                    
                    h.real /= sqrt(FN);
                    h.image /= sqrt(FN);
                    
                    ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
                    ss = 10 * log10(ss * ss);//フェージング係数
                    
                    //判定
                    
                    d = sqrt( pow(ap.x[k] - ap.x[i], 2) + pow(ap.y[k] - ap.y[i], 2));
                    
                    if(d<=D){
                        d0 = d;
                    }else{
                        d0 = D0;
                    }
                    
                    pl[i][k] = Ptx + 20*log10( (C/F) / (4.0*M_PI*d0) ) - 10 * n * log10(d/d0);
                    pl[i][k] = pl[i][k] + ss + s[i][k];//パスロス
                    
                    //シャドウイング
                    
                    
                    //  plt[i][k] = pl[i][k];
                    //printf("%d-%d;%8lf\t", i + 1, k + 1, pl[i][k]);//パスロス
                }
                //printf("\n");

            }/*else{
                for(k = 0; k < M; k++){//送信先
                    pl[i][k] = 0.0;
                }
            }*/
            //printf("%d;%d\t%8lf\t\n", i+1, b.flag[i], b.time[i]);
        
        }
        //printf("\n");
        
        //振動子　角速度計算
        for(i = 0;i<M;i++){
            for(k = 0;k<M;k++){
                if(b.flag[k] == 1 && b.time[i] > 0.0 && pl[i][k] > Pmin && pl[i][k] != 0.0 && i != k){ //
                    o[i] = o[i] + ( E * sin( Omega * ( t - b.time[i]) ) );
                    
                    pk[i][k] += 1.0;
                    //printf("time;%8lf\t%d\n", t, i+1);
                   // printf("%d\t%8lf\n", b.flag[k], pl[i][k]);
                }
            }
            
        }
        
        for(k = 0;k<M;k++){
                b.flag[k] = 0;
            }
        
    }
        
        /*
        else{
            printf("%d-%d;%d\t%8lf\n", i+1, k+1, b.flag[k], pl[i][k]);
        }
    */
    
    
//位相差表示
    
    for(j = 0;j<N;j+=1000){
        printf("%8lf\t", ( (double)j + (double)m * N ) * Dt);
        for(i = 0; i < M; i++){
            printf("%8lf\t", pd1[i][j]);
        }
        printf("\n");
    }
      
        
        
    
}
    /*
    for(i = 0; i<M; i++){
        for(j = 0; j<M;j++){
            printf("%8lf\t", plt[i][j]);
            
        }
        printf("\n");
    }
     */
    
    /*
    printf("1;%lf\t2;%lf\t3;%lf\t4;%lf\t\n\n", tb[0], tb[1], tb[2], tb[3]);
    printf("1-2;%2lf\t1-3;%2lf\t1-4;%2lf\t\n", pk[0][1], pk[0][2], pk[0][3]);
    printf("2-1;%2lf\t2-3;%2lf\t2-4;%2lf\t\n", pk[1][0], pk[1][2], pk[1][3]);
    printf("3-1;%2lf\t3-2;%2lf\t3-4;%2lf\t\n", pk[2][0], pk[2][1], pk[2][3]);
    printf("4-1;%2lf\t4-2;%2lf\t4-3;%2lf\t\n", pk[3][0], pk[3][1], pk[3][2]);
     
     */
}
    
    
    
    


  
        
    
        /*
//パスロス計算ーーーーーーーーーーーーー
        
        for(m = 0; m < 100; m++){
            d = (double)m;
            
        for(l = 0; l < ROOP; l++){
            
            
            //レイリーフェージング
        for(j = 0;j < FN;j++){
            theta[j]= genrand_real2() * 2 * M_PI;
        }
        
        for (int j = 0; j < FN; j++) {
            h.real += cos(theta[j]);
            h.image += sin(theta[j]);
        }
        
        h.real /= sqrt(FN);
        h.image /= sqrt(FN);
        
        ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
            ss = 10 * log10(ss * ss);
        
        
        
             if(d<=D){
                 d0 = d;
             }else{
                 d0 = D0;
             }
             
             pl = Ptx + 20*log10(C/F/(4.0*M_PI*d0))-10*n*log10(d/d0);
        
            pl = pl + ss;
            
            
            if(pl > Pmin){
                fl = 1;
            }else{
                fl = 0;
            }
            
            p += (double)fl;
            

        //printf("距離:%8lf\tロス:%8lf\tフェージング係数:%8lf\t\n", d, pl, ss);
        }
        
        p = p / ROOP;
        
        printf("%d\t%8lf\n", m, p);
        }
        p = 0.0;
         
         
         */
    
         //printf("\n-----------------\n");
         
         
         
         //   printf("time:%8lf\t r1:%8lf\t r2:%8lf\t phase:%8lf\t n1:%d\t n2:%d\t\n", t, r1[i], r2[i], pd[i], n1, n2);
         //  printf("%8lf\t %8lf\t %8lf\t %8lf\t %d\t %d\t\n", t, r1[i], r2[i], pd[i], n1, n2);
        //  printf("%8lf\t %8lf\t %8lf\t\n", t, o1, o2);
        
        /*
         for(j = 0;j<N;j+=99){
         printf("%8lf\t %8lf\t %8lf\t %8lf\t %8lf\t %8lf\t\n", (j + N * k)*Dt, r1[j], r2[j], pd[j], o1, o2);
         }
         */
    

/*
 //振動子1の瞬間角速度算出------------
 if(b.flag[1] == 1 && b.time[0] > 0.0 && pl[0][1] > Pmin){//ap2からのビーコン受信
 b.flag[1] = 0;
 o[0] = o[0] + ( E * sin( Omega * ( t - b.time[0]) ) );
 pk[0][1] += 1.0 ;
 }
 
 if(b.flag[2] == 1 && b.time[0] > 0.0 && pl[0][2] > Pmin){//ap3からのビーコン受信
 b.flag[2] = 0;
 o[0] = o[0] + ( E * sin( Omega * ( t - b.time[0]) ) );
 pk[0][2] += 1.0 ;
 
 }
 
 if(b.flag[3] == 1 && b.time[0] > 0.0 && pl[0][3] > Pmin){//ap4からのビーコン受信
 b.flag[3] = 0;
 o[0] = o[0] + ( E * sin( Omega * ( t - b.time[0]) ) );
 pk[0][3] += 1.0 ;
 
 }
 
 
 //振動子2の瞬間角速度算出------------
 if(b.flag[0] == 1 && b.time[1] > 0.0 && pl[1][0] > Pmin){//ap1からのビーコン受信
 b.flag[0] = 0;
 o[1] = o[1] + ( E * sin( Omega * ( t - b.time[1]) ) );
 pk[1][0] += 1.0 ;
 
 }
 
 if(b.flag[2] == 1 && b.time[1] > 0.0 && pl[1][2] > Pmin){//ap3からのビーコン受信
 b.flag[2] = 0;
 o[1] = o[1] + ( E * sin( Omega * ( t - b.time[1]) ) );
 pk[1][2] += 1.0 ;
 
 }
 
 if(b.flag[3] == 1 && b.time[1] > 0.0 && pl[1][3] > Pmin){//ap4からのビーコン受信
 b.flag[3] = 0;
 o[1] = o[1] + ( E * sin( Omega * ( t - b.time[1]) ) );
 pk[1][3] += 1.0 ;
 
 }
 
 //振動子3の瞬間角速度算出------------
 if(b.flag[0] == 1 && b.time[2] > 0.0 && pl[2][0] > Pmin){//ap1からのビーコン受信
 b.flag[0] = 0;
 o[2] = o[2] + ( E * sin( Omega * ( t - b.time[2]) ) );
 pk[2][0] += 1.0 ;
 
 }
 
 if(b.flag[1] == 1 && b.time[2] > 0.0 && pl[2][1] > Pmin){//ap2からのビーコン受信
 b.flag[1] = 0;
 o[2] = o[2] + ( E * sin( Omega * ( t - b.time[2]) ) );
 pk[2][1] += 1.0 ;
 
 }
 
 if(b.flag[3] == 1 && b.time[2] > 0.0 && pl[2][3] > Pmin){//ap4からのビーコン受信
 b.flag[3] = 0;
 o[2] = o[2] + ( E * sin( Omega * ( t - b.time[2]) ) );
 pk[2][3] += 1.0 ;
 
 }
 
 //振動子4の瞬間角速度算出------------
 if(b.flag[0] == 1 && b.time[3] > 0.0 && pl[3][0] > Pmin){//ap1からのビーコン受信
 b.flag[0] = 0;
 o[3] = o[3] + ( E * sin( Omega * ( t - b.time[3]) ) );
 pk[3][0] += 1.0 ;
 
 }
 
 if(b.flag[1] == 1 && b.time[3] > 0.0 && pl[3][1] > Pmin){//ap2からのビーコン受信
 b.flag[1] = 0;
 o[3] = o[3] + ( E * sin( Omega * ( t - b.time[3]) ) );
 pk[3][1] += 1.0 ;
 
 }
 
 if(b.flag[2] == 1 && b.time[3] > 0.0 && pl[3][2] > Pmin){//ap3からのビーコン受信
 b.flag[2] = 0;
 o[3] = o[3] + ( E * sin( Omega * ( t - b.time[3]) ) );
 pk[3][2] += 1.0 ;
 
 }

 
 */

/*
 if(j%1000 == 0){
 for(i = 0;i < M; i++){
 printf("%8lf\t", o[i]);
 }
 printf("\n");
 }
 */

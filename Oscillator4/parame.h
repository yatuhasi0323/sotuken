
//各パラメータ

//引き込み係数
#define E 0.02

//データ数
#define N 100000

//基本角速度
#define Omega 20*M_PI

//サンプリング感覚
#define Dt 0.0001

//繰り返し数
#define M 10



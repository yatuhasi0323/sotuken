#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "main.h"

int main(){
    int i, j;
    double t = 0.0;//時間
    double pd[N];//位相差

    
    int b1 = 0;//振動子1のビーコン
    double bt1 = 0.0;//振動子１のビーコンタイム
    double p1 = 0.0;//振動子1の位相差
    int n1 = 0;
    double o1 = 0.0;//振動子1の瞬時角速度
    double r1[N];//振動子1の位相
    double tr1[N];//総移動距離
    double brt1 = 0.0;//ビーコン受信タイミング

    int b2 = 0;//振動子2のビーコン
    double bt2 = 0.0;//振動子2のビーコンタイム
    double p2 = 0.0;//振動子2の位相差
    int n2 = 0;
    double o2 = 0.0;//振動子2の瞬時角速度
    double r2[N];//振動子2の位相
    double tr2[N];//総移動距離
    double brt2 = 0.0;//ビーコン受信タイミング

    
    
    init_genrand((unsigned)time(NULL));
    
    
   // p1 = 0;
    
   // p2 = M_PI / 4; //位相差 パイ/4
    
    p1 = 2*M_PI*genrand_real3();
    
    p2 = 2*M_PI*genrand_real3();

    printf("p1 = %8lf\t p2 = %8lf\t\n", p1, p2);
    
    
    o1 = Omega;//初期値
    o2 = Omega;//初期値

    
    for(i = 0;i<N;i++){
        t = i * Dt;

        //振動子1のーーーーー位相計算ーーーーーーーーー
        tr1[i] = o1 * t + p1;
        r1[i] = tr1[i] - (2 * M_PI * (double)n1);
        //printf("振動子1の位相: %8lf\t\n\n", r1[i]);
        
        //振動子2のーーーーー位相計算ーーーーーーーーー
        tr2[i] = o2 * t + p2;
        r2[i] = tr2[i] - (2 * M_PI * (double)n2);
       // printf("振動子2の位相: %8lf\t\n\n", r2[i]);
       
        //位相差計算ーーーーーーーー
        
        
        if(n1 == n2){
            pd[i] = r2[i] - r1[i];
        }else if(n1 > n2){
            pd[i] = r2[i] - (r1[i] + (2 * M_PI));
        }else if(n1 < n2){
            pd[i] = (r2[i] + (2 * M_PI)) - r1[i];
        }
   
        
        //振動子1のフラグ生成
        if(r1[i] >= 2*M_PI){
                b1 = 1; //振動子1の送信フラグ
                //o1 = Omega;//角速度リセット
                bt1 = t;
            o1 = Omega;
                n1 += 1;
        }else{
                b1 = 0;
            }
            
            //振動子2のフラグ生成
        if(r2[i] >= 2*M_PI){
            b2 = 1;//振動子２の送信フラグ
            //o2 = Omega;//角速度リセット
            bt2 = t;
            o2 = Omega;
            n2 += 1;
        }else{
            b2 = 0;
        }
        
        //振動子1の瞬間角速度算出------------
        if(b2 == 1){//ビーコン受信
            if(t > bt1){
                brt1 = t;//ビーコン受信時間の保持
            }else{
                o1 = o1 * ( E * sin( o1 * ( bt1 - t ) ) );
                brt1 = 0.0;
            }
        }
        
        if(b1 == 1){//ビーコン送信
            if(brt1 != 0.0){//受信タイミングを保持している場合
                o1 = o1 + ( E * sin( o1 * ( brt1 - t ) ) );
            }
        }
        
        
        //振動子2の瞬間角速度算出------------
        if(b1 == 1){//ビーコン受信
            if(t > bt2){
                brt2 = t;//ビーコン受信時間の保持
            }else{
                o2 = o2 + ( E * sin( o2 * ( bt2 - t ) ) );
                brt2 = 0.0;
            }
        }
        
        if(b2 == 1){//ビーコン送信
            if(brt2 != 0.0){//受信タイミングを保持している場合
                o2 = o2 + ( E * sin( o2 * ( brt2 - t ) ) );
            }
        }

    
        
        
        //printf("\n-----------------\n");
        
        
        
     //   printf("time:%8lf\t r1:%8lf\t r2:%8lf\t phase:%8lf\t n1:%d\t n2:%d\t\n", t, r1[i], r2[i], pd[i], n1, n2);
      //  printf("%8lf\t %8lf\t %8lf\t %8lf\t %d\t %d\t\n", t, r1[i], r2[i], pd[i], n1, n2);
       // printf("%8lf\t %8lf\t %8lf\t\n", t, o1, o2);
    }
    
    for(j = 0;j<N;j+=100){
          printf("%8lf\t %8lf\t %8lf\t %8lf\t\n", j*Dt, r1[j], r2[j], pd[j]);
    }
    
            
    
}

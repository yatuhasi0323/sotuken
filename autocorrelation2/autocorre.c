#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "parame.h"
#include "semi1.h"

void autocorre(matrix *ap, matrix *sp){//ap:内積を入れる箱　sp:元の配列
    int i, j, dx, dy; //i, j:基準の配列　dx,dy:偏差
    
    double b = 0.0;
    matrix c;//内積を入れる仮配列 L*L配列
    
    
    
    
    
    
    init(&c);//cの初期化
    
            //dx, dyの幅はspの幅と同じになる
for(dx = -(N-1);dx < N;dx++){//x軸の偏差
    for(dy = -(N-1);dy < N;dy++){//y軸の偏差
        
        //-------偏差dx、xdyでの内積計算
        
        b = 0.0;//bの初期化
        for(i=0;i<N;i++){//元の配列のx軸シフト
            for(j=0;j<N;j++){//元の配列のy軸シフト
                    
                    if(i - dx < N && i - dx >= 0 && j - dy < N && j - dy >=0){//範囲外かどうかの判定

                        c.square[i][j] = sp->square[i][j] * sp->square[i-dx][j-dy];//各要素での内積計算
                        
                        b += 1.0;
                    
                    }else{
                        
                        c.square[i][j] = 0.0;//範囲外は0

                    }
                ap->subsquare[N-(dx+1)][N-(dy+1)] += c.square[i][j]; //内積計算、正規化
    
                }
            }
        
       //   printf("%lf\t\n",b);
        ap->subsquare[N-(dx+1)][N-(dy+1)] = ap->subsquare[N-(dx+1)][N-(dy+1)] / b;
        
        }
    }
    
}
    
    
    
    
    /*
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            c[i][j] = 0;
        }
    }
    
for(n=L-1;n>=-(L-1);n--){//相関　行
    for(m=L-1;m>=-(L-1);m--){//相関　列
                    
        for(i=0;i<L;i++){//相関　行
            for(j=0;j<L;j++){//相関　列
                            
                    
                if(i+n < L && j+m < L && i + n >= 0 && j + m >= 0){
                    c[n+L-1][m+L-1] += sp->square[i][j] * sp->square[i + n][j + m];
                    }else{
                        c[n+L-1][m+L-1] += 0;
                    }
                }
            }
        c[n+L-1][m+L-1] = c[n+L-1][m+L-1] / (L*L);// 総数でわる

        printf("%8lf\t",c[n+L-1][m+L-1]);
      //  cn += c[n+L-1][m+L-1];
        
        //    printf("\n\n");
        //    printf("c合計:%f\n", cn);
        //    cn = 0.0;//初期化
        
        }
    printf("\n");

    }
    
    for(i=0;i<L;i++){//相関　行
        for(j=0;j<L;j++){//相関　列
    printf("%8lf\t",ap->square[i][j]);
        }
    printf("\n");
    }
   */


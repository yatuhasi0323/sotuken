#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "semi2.h"

int main(){
    int i, j;
    rayleigh  h;
    double theta[FN];
    double ss[ROOP];
    double ave = 0.0;

    init_genrand((unsigned)time(NULL));
    
    for(j = 0;j < ROOP;j++){
        
    for(i = 0;i < FN;i++){
        theta[i]= genrand_real2() * 2 * M_PI;
    }
    
    for (int i = 0; i < FN; i++) {
        h.real += cos(theta[i]);
        h.image += sin(theta[i]);
    }
        
        h.real /= sqrt(FN);
        h.image /= sqrt(FN);
    
    ss[j] = ALPHA * sqrt(h.real * h.real + h.image * h.image);
    
 //   printf("%8lf\n", ss[j]);
        
        ave += ss[j];
    }
    
    ave = ave/ROOP;
    
    printf("%8lf\n", ave);
    
}


//各パラメータ

//引き込み係数
#define E 0.05

//データ数
#define N 100000

//基本角速度
#define Omega 20*M_PI

//サンプリング感覚
#define Dt 0.001

//ノード数
#define M 3

//端末間距離(m)
#define D 15

//周波数(Hz)
#define F 2.4e9

//平均雑音電力(dBm)
#define N0 -90

//送信電力(dBm)
#define Ptx 17

#define D0  1.0

//伝搬係数
#define n 3.5

//最低受信電力
#define Pmin -70

//光速
#define  C 2.98e8

#define  Dm 100

//素波数
#define FN 64

//係数
#define ALPHA 1/0.895

//ループ数
#define ROOP 10

//波数
#define WN 500

//de-correlation distance (5m:都市 20m:田舎)
#define Dcor 20

//アルファ
#define Alpha log(2)/Dcor





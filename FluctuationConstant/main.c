#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "main.h"

int main(){
    int i, j, k, l;

    point ap;//アクセスポインと
    
    double d[M][M];//端末間距離
    double Apl[M][M];//平均パスロス
    double AApl[M][M];//平均パスロス
    double d0  = 0.0;
    rayleigh  h;
    double thetap[FN];
    double ss = 0.0;//フェージング係数
    
    //シャドウイングの変数
    double s[M][M];//シャドウイング係数
    double cn = 0.0;//係数
    double a = 2.0/WN;
    double fx[WN/2];
    double fy[WN/2];
    double fu[WN/2];
    double fv[WN/2];
    
    frequency fn;//Fn
    
    double beta = 0.0;//確率変数
    double phi = 0.0;
    double thetas[WN];

    double fe = 0.0;//変動引き込み係数
    
    double ans[M][M];
    
    init_genrand((unsigned)time(NULL));
    
    //シャドウイングの値
    cn = sqrt(a);
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
        //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
        // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
    for(i=0;i<WN/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(WN/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(WN/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(WN/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(WN/2)] = fy[i];
    }
    
    for(i = 0;i<WN/2;i++){
        thetas[i] = 2 * M_PI * genrand_res53();
        thetas[i+WN/2] = thetas[i];
    }
    

    

    
    //printf("初期位相\t");
  //初期化
    for(i = 0; i < M; i++){
        for(j = 0;j<M;j++){
            d[i][j] = 0.0;
            Apl[i][j] = 0.0;
            AApl[i][j] = 0.0;
            ans[i][j] = 0.0;
        }
    }
    
    //printf("\n");
//  ap配置 乱数
//            for(j=0; j<M; j++){
//                ap.x[j] = Dm * genrand_real3();
//                ap.y[j] = Dm * genrand_real3();
//            }
    //ap配置 乱数づらし
    
//        for(j=0; j<5; j++){
//            for(k=0; k<5; k++){
//                ap.x[(j*5)+k] = k * D + 5 * genrand_real3();
//               // ap.x[(j*5)+k] = k * D;
//
//                ap.y[(j*5)+k] = j * D + 5 * genrand_real3();
//               // ap.y[(j*5)+k] = j * D;
//
//                printf("%lf\t%lf\n", ap.x[(j*5)+k], ap.y[(j*5)+k]);
//            };
//            // printf("\n");
//        };
    
    //ap配置 9個
    
    //ap.x[4] = 5 * genrand_real3();
    //ap.y[4] = 5 * genrand_real3();

            for(j=0; j<5; j++){
                for(k=0; k<5; k++){
                    ap.x[(j*5)+k] += k * D;
                   // ap.x[(j*5)+k] = k * D;
    
                    ap.y[(j*5)+k] += j * D;
                   // ap.y[(j*5)+k] = j * D;
    
                   // printf("%lf\t%lf\n", ap.x[(j*5)+k], ap.y[(j*5)+k]);
                };
                // printf("\n");
            };
    
    
   //printf("\n\n");

    
    //単体パスロス
    for(i = 0;i<M;i++){
        for(j = 0;j< M;j++){
            //シャドウイング係数の算出

            for(k = 0;k<WN;k++){
                s[i][j] += cn * cos( 2 * M_PI * (fn.x[k] * ap.x[i] + fn.y[k] * ap.y[i] + fn.u[k] * ap.x[j] + fn.v[k] * ap.y[j] ) + thetas[k]);
            }

            //printf("%8lf\t",s.square[i+L][j+L]); //データ表示
        }
        //printf("\n");
    }

  // for(m = 0; m < 1000; m++){
    for(i = 0;i < M;i++){
        for(k = 0; k < M; k++){//送信先

        //レイリーフェージング
        for(l = 0; l < FN; l++){
            thetap[l]= genrand_real2() * 2 * M_PI;
        }
        //初期化
        h.real = 0.0;
        h.image = 0.0;

        for (l = 0; l < FN; l++) {
            h.real += cos(thetap[l]);
            h.image += sin(thetap[l]);
        }

        h.real /= sqrt(FN);
        h.image /= sqrt(FN);

        ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
        ss = 10 * log10(ss * ss);//フェージング係数

        //判定

        d[i][k] = sqrt( pow(ap.x[k] - ap.x[i], 2) + pow(ap.y[k] - ap.y[i], 2));

        if(d[i][k]>=D0){
            d0 = d[i][k];
        }else{
            d0 = D0;
        }

        Apl[i][k] = Ptx + 20*log10( (C/F) / (4.0*M_PI*d0) ) - 10 * n * log10(d[i][k]/d0);
        Apl[i][k] = Apl[i][k] + ss + s[i][k];//パスロス

            AApl[i][k] += Apl[i][k];
           //printf("%8lf\t", d[i][k]);
        //printf("%8lf\t", Apl[i][k]);//パスロス
        }
        //printf("\n");
    }

// }//パスロスループ


    //pl平均
//    for(i = 0;i < M;i++){
//        for(k = 0; k < M; k++){//送信先
//            ans[i][k] = AApl[i][k] / 1000;
//            printf("%8lf\t", ans[i][k]);
//            //printf("%8lf\t", d[i][k]);
//        }
//        printf("\n");
//    }




  //  printf("\n");
    //fe = E * ( 1.0 - ( i*1.0 / Pmin ) );//変動引き込み係数計算

    //変動引き込み係数(2次関数)
    for(i = -80;i<-50; i++){
        if(Pmin > i){
        fe = E * ( 1 / sqrt( abs(Pmin - i) ) );
        }else{
            fe = E;
        }
        printf("%d\t%8lf\n", i, fe);
    }





}

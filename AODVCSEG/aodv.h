/*
#ifndef __aodv
#define __aodv
*/

//端末の構造体
typedef struct {
    double x[N];//x座標
    double y[N];//y座標
    int id[N];//ノードアドレス
    int ch[N];//使用チャネル
}point;

//２次元配列の構造体
typedef struct {
    double square[N][N];
}matrix;

//空間周波数
typedef struct {
    double x[WN];
    double y[WN];
    double u[WN];
    double v[WN];
}frequency;

typedef struct {
    double real;
    double image;
}rayleigh;

typedef struct {
    int rid;//送信元アドレス
    int pid;//送信先アドレス
    int hop;//ホップ数
    int rflag;//受信フラグ
    int tflag;//送信フラグ 0;送信したことない、1;送信していいよ、2;送信しました
    double p;//sn比較用
}hedder;

typedef struct {
    int dominance;//優先度
    double cpw;//使用電力
}chanel;

void distance(matrix *dp,point *xp);
void passloss(matrix *sp, matrix *dp);
void shadowing(matrix *ans, point *ap, double *theta, frequency *f, double c);




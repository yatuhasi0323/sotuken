#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "aodv.h"

//乱数作るやつ
int main(){
    int i=0, j=0, l=0, k=0;
    
    int rout1_Max = 0;
    //int rout2_Max = 0;
    
    point Tx;//端末座標およびアドレス
    matrix dis;//端末間距離
    
    int sid1 = 9;//sorce1のid
    int did1 = 40;//destination1のid
    
    //int sid2 = 2;//sorce2のid
    //int did2 = N-1;//destination2のid
    
   // int count = 0;//カウンタ
    
    
    matrix pl;//パスロス
    rayleigh  h;
    double thetap[FN];
    double ss = 0.0;//フェージング係数
    
    chanel ch1[N][CN];//1ルートのチャネルの構造体
    chanel ch2[N][CN];//２ルートのチャネルの構造体

    
    double pmin;//最小値
    
    //シャドウイングの変数
    matrix s;//シャドウイング係数
    double cn = 0.0;//係数
    double a = 2.0/WN;
    double fx[WN/2];
    double fy[WN/2];
    double fu[WN/2];
    double fv[WN/2];
    
    frequency fn;//Fn
    
    double beta = 0.0;//確率変数
    double phi = 0.0;
    double thetas[WN];

    hedder rreq[N][2];
    
    int rout[MR][2];
    
    // double p[N];
  //  double g[N];
    
    
    init_genrand((unsigned)time(NULL));
    
//座標割当
    
//    //Sorceの座標
//    Tx.x[0] = -L/2;
//    Tx.y[0] = L/2;
//    Tx.id[0] = 1;
//
//    Tx.x[1] = -L/2;
//    Tx.y[1] = -L/2;
//    Tx.id[1] = 2;
//
//    //Destinationの座標
//    Tx.x[N-1] = L/2;
//    Tx.y[N-1] = -L/2;
//    Tx.id[N-1] = N;
//
//    Tx.x[N-2] = L/2;
//    Tx.y[N-2] = L/2;
//    Tx.id[N-2] = N-1;

    
//    //relyの座標
//    for(i = 2;i<N-2;i++){
//        Tx.x[i] = L * (genrand_res53()-0.5);
//        Tx.y[i] = L * (genrand_res53()-0.5);
//        Tx.id[0] = i+1;
//        Tx.ch[i] = 0;//使用チャネル　使ってない：０
//    }
    
    for(j=0; j<(int)sqrt(N); j++){
        for(k=0; k<(int)sqrt(N); k++){
            Tx.x[(j*(int)sqrt(N))+k] = k * D + sqrt((double)N) * genrand_real3();
            // ap[(j*5)+k].x = k * D;
            
            Tx.y[(j*(int)sqrt(N))+k] = j * D + sqrt((double)N) * genrand_real3();
            
            // ap[(j*5)+k].y = j * D;
                    Tx.id[(j*(int)sqrt(N))+k] = (j*(int)sqrt(N))+k +1;
                    Tx.ch[(j*(int)sqrt(N))+k] = 0;//使用チャネル　使ってない：０
            
            printf("%lf\t%lf\t%d\n", Tx.x[(j*(int)sqrt(N))+k], Tx.y[(j*(int)sqrt(N))+k], Tx.id[(j*(int)sqrt(N))+k]);
        };
    };
    
    
    //端末間距離
    distance(&dis, &Tx);
    
    
    
    //初期化
    for(i=0;i<N;i++){
        for(j = 0;j < 1;j++){
            rreq[i][j].rid = 0;
            rreq[i][j].pid = 0;
            rreq[i][j].hop = 0;
            rreq[i][j].rflag = 0;
            rreq[i][j].tflag = 0;
        }
        for(j = 0;j<CN;j++){
            ch1[i][j].cpw = 0.0;
            ch2[i][j].cpw = 0.0;
        }
        }
        

    //シャドウイングの定数ーーーーーーーーーーーー
    cn = sqrt(a);
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
        //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
        // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
    for(i=0;i<WN/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(WN/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(WN/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(WN/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(WN/2)] = fy[i];
    }
    
    for(i = 0;i<WN/2;i++){
        thetas[i] = 2 * M_PI * genrand_res53();
        thetas[i+WN/2] = thetas[i];
    }
    //--------------------------
    
    //シャドウイング算出
    shadowing(&s, &Tx, thetas, &fn, cn);
    
    //レイリーフェージング算出
        for(l = 0; l < FN; l++){
            thetap[l]= genrand_real2() * 2 * M_PI;
        }
        //初期化
        h.real = 0.0;
        h.image = 0.0;
        
        for (l = 0; l < FN; l++) {
            h.real += cos(thetap[l]);
            h.image += sin(thetap[l]);
        }
        
        h.real /= sqrt(FN);
        h.image /= sqrt(FN);
        
        ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
        ss = 10 * log10(ss * ss);//フェージング係数
        
        passloss(&pl, &dis);

    double df[N][N];
        for(i=0;i<N;i++){
            for(j=0;j<N;j++){
                df[i][j] = pl.square[i][j] + ss + s.square[i][j];
        //pl.square[i][j] = pl.square[i][j] + ss + s.square[i][j];//パスロス
                pl.square[i][j] = pow(10.0, (df[i][j] / 10.0));//パスロス

            }
        }
    
    
    //パスロス表示ーーーーーーーーーーーーーー
    /*
    for(i=0;i<N;i++){
        for(j=0;j<N;j++){
            printf("%8lf\t", pl.square[i][j]);
        }
            printf("\n");
    }
    */
    
    
    //ルーティング
  
//    for(j=0;j<N;j++){
//        printf("%d\t%lf\t%lf\t\n",j+1, Tx.x[j], Tx.y[j]);
//    }
    
    //1hop目
        for(j=0;j<N;j++){
            if(pl.square[sid1-1][j] > Pmin){//sorceと他との通信可能判定
                rreq[j][0].rid = sid1;
                rreq[j][0].pid = did1;
                rreq[j][0].hop +=1;
                rreq[j][0].rflag = 1;//sourceから受信しましたフラグ
        }
        }
    
    
//2hop以降
//
    while(rreq[N-1][0].pid != did1){//Dにパスが届くまでループ


    for(i = 1;i<N;i++){
    if(rreq[i][0].rflag == 1 && rreq[i][0].tflag == 0){//受信したか、すでに送信しているかの判定
        rreq[i][0].tflag = 1;//送信フラグ
    }
    }
    
    
    for(i = 1;i<N;i++){
        for(j = 1;j<N;j++){

        if(pl.square[i][j] > Pmin && rreq[i][0].tflag == 1 && rreq[j][0].tflag == 0){//受信可能かどうか
            if(rreq[j][0].rflag == 1){//受信側がすでに受信したことがあるかどうか(hop数が同じ場合のみ)
                if(rreq[j][0].p < pl.square[i][j]){
                    rreq[j][0].rid = i+1;
                    rreq[j][0].pid = did1;
                    rreq[j][0].hop = rreq[i][0].hop+1;
                    rreq[j][0].rflag = 1;
                    rreq[j][0].p = pl.square[i][j];
                }
            }else{
                
                rreq[j][0].rid = i+1;
                rreq[j][0].pid = did1;
                rreq[j][0].hop = rreq[i][0].hop+1;
                rreq[j][0].rflag = 1;
                rreq[j][0].p = pl.square[i][j];
            }
            
            }
        }
    }
    }
    
   // printf("%d\t\n", count);
    /*
            for(j=0;j<N;j++){
                printf("%d\t%d\t%d\t%d\t%d\t%lf\t%lf\t\n",j+1, rreq[j][0].rid, rreq[j][0].pid, rreq[j][0].hop, rreq[j][0].rflag, Tx.x[j], Tx.y[j]);
            }
    */
    
    //経路決定
    for(j = 0;j<MR;j++){
        rout[j][0] = 0;
    }
   
    rout[rreq[N-1][0].hop][0] = did1;
    
    for(i = rreq[N-1][0].hop; i > 0 ; i--){
        j = rout[i][0];
        //printf("%d\n",j);
        rout[i-1][0] = rreq[j-1][0].rid;
    }
    printf("\n");

    for(i = 0;i<MR;i++){
    printf("%d\n",rout[i][0]);
    }
    
    rout1_Max = rreq[N-1][0].hop+1;
    printf("%d\n", rout1_Max);
    // ２つめのルート
    //1hop目
//    for(j=0;j<N;j++){
//        if(pl.square[sid2-1][j] > Pmin){//sorceと他との通信可能判定
//            rreq[j][1].rid = sid2;
//            rreq[j][1].pid = did2;
//            rreq[j][1].hop +=1;
//            rreq[j][1].rflag = 1;//sourceから受信しましたフラグ
//        }
//    }
//
//
//    //2hop以降
//    //
//    while(rreq[N-2][1].pid != did2){//Dにパスが届くまでループ
//
//
//        for(i = 1;i<N;i++){
//            if(rreq[i][1].rflag == 1 && rreq[i][1].tflag == 0){//受信したか、すでに送信しているかの判定
//                rreq[i][1].tflag = 1;//送信フラグ
//            }
//        }
//
//
//        for(i = 1;i<N;i++){
//            for(j = 1;j<N;j++){
//
//                if(pl.square[i][j] > Pmin && rreq[i][1].tflag == 1 && rreq[j][1].tflag == 0){//受信可能かどうか
//                    if(rreq[j][1].rflag == 1){//受信側がすでに受信したことがあるかどうか(hop数が同じ場合のみ)
//                        if(rreq[j][1].p < pl.square[i][j]){
//                            rreq[j][1].rid = i+1;
//                            rreq[j][1].pid = did2;
//                            rreq[j][1].hop = rreq[i][1].hop+1;
//                            rreq[j][1].rflag = 1;
//                            rreq[j][1].p = pl.square[i][j];
//                        }
//                    }else{
//
//                        rreq[j][1].rid = i+1;
//                        rreq[j][1].pid = did2;
//                        rreq[j][1].hop = rreq[i][1].hop+1;
//                        rreq[j][1].rflag = 1;
//                        rreq[j][1].p = pl.square[i][j];
//                    }
//
//                }
//            }
//        }
//    }
//
  //  printf("%d\t\n", count);
    /*
    for(j=0;j<N;j++){
        printf("%d\t%d\t%d\t%d\t%d\t%lf\t%lf\t\n",j+1, rreq[j][1].rid, rreq[j][1].pid, rreq[j][1].hop, rreq[j][1].rflag, Tx.x[j], Tx.y[j]);
    }
    */
    
    //経路決定
//    for(j = 0;j<MR;j++){
//        rout[j][1] = 0;
//    }
//
//    rout[rreq[N-2][1].hop][1] = did2;
//
//    for(i = rreq[N-2][1].hop; i > 0 ; i--){
//        j = rout[i][1];
//        //printf("%d\n",j);
//        rout[i-1][1] = rreq[j-1][1].rid;
//    }
//    printf("\n");
//
//    for(i = 0;i<MR;i++){
//        printf("%d\n",rout[i][1]);
//    }
//
//    rout2_Max = rreq[N-2][1].hop + 1;
//    printf("%d\n", rout2_Max);

    
    
    //チャネル住み分け
    
    //ルート1のチャネル

    Tx.ch[ rout[0][0] - 1 ] = 1;//最初 ch1を使用

    double df2[rout1_Max][CN];
    for(i = 1;i<rout1_Max;i++){
        for(j = 0;j<CN;j++){
            df2[i][j] = 0.0;
        }
    }
    
    
    for(i = 1;i<rout1_Max;i++){//ノード選択ループ>ルートから持ってくる
        for(j = 0;j<CN;j++){//チャネル選択ループ
            for(k = 0;k<rout1_Max;k++){//同一チャネル選別ループ
                if(Tx.ch[rout[k][0] - 1] == j+1 && i != k){//チャネル使用ノード検索 どうチャネルである場合
                    df2[i][j] += pl.square[rout[i][0] - 1][k];//チャネル消費電力の総和
                }else{
                    df2[i][j] += 0.0;
                }
            }
            //printf("%16.10lf\t", df2[i][j]);
            ch1[i][j].cpw = df2[i][j];
        }
        //printf("\n");
        
        //if(Tx.ch[rout[i][0] - 1] == 0){
            pmin = ch1[i][0].cpw;
            Tx.ch[rout[i][0] - 1] = 1;
            for(j = 1;j<CN;j++){
                if(pmin > ch1[i][j].cpw){
                    Tx.ch[rout[i][0] - 1] = j + 1;
                    pmin = ch1[i][j].cpw;
                }
            }
       // }
        
        
    }
    
    for(i = 0;i<rout1_Max;i++){
        printf("%d\t%d\n", Tx.ch[rout[i][0] - 1], rout[i][0]);
    }
    
    for(i = 0;i<rout1_Max;i++){
        for(j = 0;j<CN;j++){
            printf("%16.15lf\t", ch1[i][j].cpw);
        }
        printf("\n");
    }
    
    
    //2ルート目の方----------------------------------
    
    //Tx.ch[ rout[0][1] - 1 ] = 1;//最初 ch1を使用
    
//    double df3[rout2_Max][CN];
//    for(i = 1;i<rout2_Max;i++){
//        for(j = 0;j<CN;j++){
//            df3[i][j] = 0.0;
//        }
//    }
//
//
//    for(i = 0;i<rout2_Max;i++){//ノード選択ループ>ルートから持ってくる
//        for(j = 0;j<CN;j++){//チャネル選択ループ
//            for(k = 0;k<rout2_Max;k++){//同一チャネル選別ループ
//                if(Tx.ch[rout[k][1] - 1] == j+1 && i != k){//チャネル使用ノード検索 どうチャネルである場合
//
//                    df3[i][j] += pl.square[rout[i][1] - 1][k];//チャネル消費電力の総和
//                }else{
//                    df3[i][j] += 0.0;
//                }
//            }
//            //printf("%16.10lf\t", df2[i][j]);
//            ch2[i][j].cpw = df3[i][j];
//        }
//        //printf("\n");
//
//        //if(Tx.ch[rout[i][1] - 1] == 0){
//            pmin = ch2[i][0].cpw;
//            Tx.ch[rout[i][1] - 1] = 1;
//            for(j = 1;j<CN;j++){
//                if(pmin > ch2[i][j].cpw){
//                    Tx.ch[rout[i][1] - 1] = j + 1;
//                    pmin = ch2[i][j].cpw;
//                }
//            }
//       // }
//
//
//    }
//    printf("\n\n");
//
//    for(i = 0;i<rout2_Max;i++){
//        printf("%d\t%d\n", Tx.ch[rout[i][1] - 1], rout[i][1]);
//    }
//
//    for(i = 0;i<rout2_Max;i++){
//        for(j = 0;j<CN;j++){
//            printf("%16.15lf\t", ch2[i][j].cpw);
//        }
//        printf("\n");
//    }
    
    
    
    
    
        
}

    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    

    
        
        //  plt[i][k] = pl[i][k];
        //printf("%d-%d;%8lf\t", i + 1, k + 1, pl[i][k]);//パスロス
    
    //printf("\n");
    /*else{
  for(k = 0; k < M; k++){//送信先
  pl[i][k] = 0.0;
  }
  }*/
//printf("%d;%d\t%8lf\t\n", i+1, b.flag[i], b.time[i]);


    



   
        
        //座標表示ーーーーーーーーーー
        /*
        for(k = 0; k<N;k++){
            printf("%d, x=%lf, y = %lf\n", k, Tx.x[k], Tx.y[k]);
        }
         */
        
        

        
        
     //





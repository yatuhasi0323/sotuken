
//各パラメータ

//基本引き込み係数
#define E 0.5

//データ数
#define N 100000

//基本角速度
#define Omega 20*M_PI

//最低角速度
#define OmegaMin 2*M_PI

//サンプリング感覚
#define Dt 0.001//0.001

//ノード数
#define M 25

//端末間距離(m)
#define D 25

//周波数(Hz)
#define F 2.4e9

//平均雑音電力(dBm)
#define N0 -90

//送信電力(dBm)
#define Ptx 4

#define D0  1.0

//伝搬係数
#define n 3.5

//最低受信電力
#define Pmin -70

#define Pmax -50

//光速
#define  C 2.98e8

#define  Dm 100

//素波数
#define FN 64

//係数
#define ALPHA 1/0.895

//ループ数
#define ROOP 100

//波数
#define WN 500

//de-correlation distance (5m:都市 20m:田舎)
#define Dcor 5

//アルファ
#define Alpha log(2)/Dcor

//平滑化定数
#define A  0.1

#define K 3 //グループ数

#define KLOOP 10//kmeansの繰り返し数

#define TLOOP 1//総合ループ

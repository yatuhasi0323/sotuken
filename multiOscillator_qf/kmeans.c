#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "parame.h"
#include "main.h"

void kmeans(point *pap, point *centers){

    int i, j, l;
    
    double dis[M][K];//各Apと中心点の距離
    
    double prodis[M];//最短距離判定のための仮箱
    double gnum[K];//所属Ap数
    
    point gtotal[K];//所属Apのx,y座標の総和
    point gave[K];//所属Apのx,y座標の平均
    
    //初期化
    for(i = 0;i < M;i++){
        prodis[i] = 0.0;
        for(j = 0;j < K;j++){
            dis[i][j] = 0.0;
        }
    }
    
    for(j = 0;j < K;j++){
        gtotal[j].x = 0.0;
        gtotal[j].y = 0.0;
        gave[j].x = 0.0;
        gave[j].y = 0.0;
        gnum[j] = 0.0;
    }

    for(j = 0; j < K;j++){
       // printf("%8lf\t%8lf\t%8lf\n", (cph[j] * 180) / M_PI, centers[j].x, centers[j].y);//centers表示
    }
    
    
    //距離計算
    
    for(l = 0;l < KLOOP;l++){//繰り返しループ
        
        for(i = 0;i < M;i++){//初期化
            prodis[i] = 0.0;
            for(j = 0;j < K;j++){
                dis[i][j] = 0.0;
            }
        }//End Loop初期化
        for(i = 0;i < M;i++){//距離計算Ap
            for(j = 0;j < K;j++){//中心点
                
                dis[i][j] =  sqrt( pow(centers[j].x - pap[i].x, 2) + pow(centers[j].y - pap[i].y, 2));
                
                //printf("Ap:%d-centers:%d\tdis:%8lf\n",i, j, dis[i][j]);//距離表示
                
                if(prodis[i] == 0 || prodis[i] > dis[i][j]){//最短判定
                    prodis[i] = dis[i][j];
                    pap[i].g = j;//所属グループ割当て
                }
                
            }
            
            //printf("\ngroup:%d\tmin:%8lf\n\n",group[i], prodis[i]);
            
        }//End Loop 距離計算AP
        
        
        //centers移動
        
        //printf("\n\n中心点更新\n\n");//更新
        for(j = 0;j < K;j++){//所属Ap平均
            for(i = 0;i < M;i++){//Ap
                
                if(pap[i].g == j){
                    gtotal[j].x += pap[i].x;
                    gtotal[j].y += pap[i].y;
                    gnum[j] += 1.0;
                }
            }
            gave[j].x = gtotal[j].x / gnum[j];
            gave[j].y = gtotal[j].y / gnum[j];
            //printf("%8lf\t%8lf\n\n", gave[j].x, gave[j].y);//平均座標の表示
            
            centers[j].x = ( gave[j].x) / sqrt( pow(gave[j].x, 2) + pow(gave[j].y, 2) );
            centers[j].y = ( gave[j].y) / sqrt( pow(gave[j].x, 2) + pow(gave[j].y, 2) );
            //printf("%8lf\t%8lf\n", centers[j].x, centers[j].y);//更新されたcentersの表示
        }//End Loop 所属Ap平均
        
        
    }//End Loop 繰り返しループ
    
    for(j = 0;j < K;j++){//割当後表示
        //sprintf("%8lf\t%8lf\n\n", centers[j].x, centers[j].y);
        
        for(i = 0;i < M;i++){//Ap
            if(pap[i].g == j){
                //printf("%8lf\t%8lf\t%d\n", pap[i].x, pap[i].y, j);
            }
        }
        //printf("\n");
    }//End Loop 割当後表示
   
}

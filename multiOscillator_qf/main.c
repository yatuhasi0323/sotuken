#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "MT.h"
#include "parame.h"
#include "main.h"

int main(){
    int i, j, k, l, m, re;
    double t = 0.0;//時間
    double pd1[M][N];//1基準の位相差
    //double pd4[M][N];//4基準の位相差
    double cph[K];//中心点の位相

    point ap[M];//アクセスポイント
    point apph[M];//引き込み後のApの位相
    point centers[K];//中心点の座標

    beacon b;//ビーコン
    
    double o[M];//振動子の瞬時角速度
    double theta[M];//振動子の位相(リセットかけてる方)
    double total[M];//振動子の位相
    double r[M][N];//振動子の位相配列    
    
    double d[M][M];//端末間距離
    double pl[M][M];//パスロス
    double Apl[M][M];//平均パスロス
    double AApl[M][M];//平均パスロス
    double d0  = 0.0;
    rayleigh  h;
    double thetap[FN];
    double ss = 0.0;//フェージング係数
    
    //シャドウイングの変数
    double s[M][M];//シャドウイング係数
    double cn = 0.0;//係数
    double a = 2.0/WN;
    double fx[WN/2];
    double fy[WN/2];
    double fu[WN/2];
    double fv[WN/2];
    
    frequency fn;//Fn
    
    double beta = 0.0;//確率変数
    double phi = 0.0;
    double thetas[WN];

    
    double pk[M][M];//パケット受信回数
    int tb[M];//ビーコン発信回数
    
    double btime[M][M];//それぞれの端末からのビーコン受信タイミング
    
    double fe = 0.0;//変動引き込み係数
    
    double w[M][M];//指数平滑移動平均
    
    point ave[K];
    double gnum[K];
    point disp[ROOP];
    double avedisp[ROOP];
    double mindisp = 0.0;
    int result = 0;
    double group[M][ROOP];
    
    double val[M];//引き込み値
    
    double gpl[M][TLOOP];//同じチャネルのApから受ける電力
    double chpl[K][TLOOP];//チャネルの利用電力の平均
    double totalgpl[K][TLOOP];//チャネルの総合利用電力
    
    double avechpl[K];
    double avetotalgpl[K];
    double chdisp[K];
    double totalgdisp[K];
    double gnum2[K][TLOOP];


    
    init_genrand((unsigned)time(NULL));
    
    //シャドウイングの値
    cn = sqrt(a);
    
    
    for(j = 0;j < K;j++){
        avechpl[j] = 0.0;
        avetotalgpl[j] = 0.0;
        chdisp[j] = 0.0;
        totalgdisp[j] = 0.0;
        for(re = 0;re < TLOOP;re++){
            chpl[j][re] = 0.0;
            totalgpl[j][re] = 0.0;
            gnum2[j][re] = 0.0;
        }
    }

    
    
    for(re = 0;re < TLOOP;re++){//総合繰り返しループ

    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fx[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fx[i] = fabs(fx[i]) * cos(phi);
        
        fy[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fy[i] = fabs(fy[i]) * sin(phi);
        
        //  printf("fx=%lf,fy=%lf\n",fx[i], fy[i]);
    }
    
    for(i=0;i<WN/2;i++){
        beta = genrand_real1();
        phi = 2 * M_PI * genrand_res53();
        //  printf("beta=%lf,phi=%lf\n",beta, phi);
        
        fu[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 /( ( (1 - beta) * (1 - beta) ) )) - 1 );
        fu[i] = fabs(fx[i]) * cos(phi);
        
        fv[i] = ( Alpha / (2 * M_PI) ) * ( sqrt( 1 / ( (1 - beta) * (1 - beta) ) )  - 1 );
        fv[i] = fabs(fy[i]) * sin(phi);
        // printf("fu=%lf,fv=%lf\n",fu[i], fv[i]);
    }
    
    for(i=0;i<WN/2;i++){
        fn.x[i] = fx[i];
        fn.x[i+(WN/2)] = fu[i];
        
        fn.y[i] = fy[i];
        fn.y[i+(WN/2)] = fv[i];
        
        fn.u[i] = fu[i];
        fn.u[i+(WN/2)] = fx[i];
        
        fn.v[i] = fv[i];
        fn.v[i+(WN/2)] = fy[i];
    }
    
    for(i = 0;i<WN/2;i++){
        thetas[i] = 2 * M_PI * genrand_res53();
        thetas[i+WN/2] = thetas[i];
    }
    

    

    
    //printf("初期位相\t");
  //初期化
    for(i = 0; i < M; i++){
        o[i] = Omega;//初期加速度
        ap[i].g = 0;
        val[i] = 0.0;
        gpl[i][re] = 0.0;
        theta[i] = 2*M_PI*genrand_real3();//初期位相
       // theta[i] = 0.0;
        total[i] = theta[i];      
          //printf("%8lf\t", theta[i]);
        
        b.flag[i] = 0;
        b.time[i] = 0.0;
        tb[i] = 0;
        
        apph[i].x = 0.0;
        apph[i].y = 0.0;
        
        for(j = 0;j<M;j++){
            d[i][j] = 0.0;
            Apl[i][j] = 0.0;
            AApl[i][j] = 0.0;
            w[i][j] = 0.0;
            btime[i][j] = 0.0;
        }
    }

    

    
    //printf("\n");
//  ap配置 乱数
//            for(j=0; j<M; j++){
//                ap[j].x = Dm * genrand_real3();
//                ap[j].y = Dm * genrand_real3();
//            }
    //ap配置 乱数づらし
    
        for(j=0; j<5; j++){
            for(k=0; k<5; k++){
                ap[(j*5)+k].x = k * D + 5 * genrand_real3();
               // ap[(j*5)+k].x = k * D;

                ap[(j*5)+k].y = j * D + 5 * genrand_real3();
               // ap[(j*5)+k].y = j * D;

                //printf("%lf\t%lf\n", ap[(j*5)+k].x, ap[(j*5)+k].y);
            };
            // printf("\n");
        };
    
    //ap配置 9個
    
    //ap[4].x = 5 * genrand_real3();
    //ap[4].y = 5 * genrand_real3();
    
//    for(i = 0;i<5;i++){
//        ap[5+i].x += D/2;
//        ap[15+i].x += D/2;
//    }
    
//規則配置
//    ap[12].x = 4;
//    ap[12].y = 6;
//            for(j=0; j<5; j++){
//                for(k=0; k<5; k++){
//                    ap[(j*5)+k].x += k * D;
//                   // ap[(j*5)+k].x = k * D;
//
//                    ap[(j*5)+k].y += j * D;
//                   // ap[(j*5)+k].y = j * D;
//
//                    printf("%lf\t%lf\n", ap[(j*5)+k].x, ap[(j*5)+k].y);
//                };
//                // printf("\n");
//            };
    
    
   //printf("\n\n");


    
    for(m = 0; m < ROOP; m++){

        for(j = 0;j<N;j++){
            for(i = 0; i < M; i++){
                pd1[i][j] = 0.0;
                //pd4[i][j] = 0.0;
            }
        }

    for(j = 0;j<N;j++){//メインループ
        t = ( (double)j + (double)m * N ) * Dt;//サンプリングタイム

         //振動子の位相計算ーーーーーーーーーーーーーー
        for(i = 0; i < M; i++){//送信側のAp番号
             if(j != 0){
             theta[i] = theta[i] + o[i] * Dt;
             total[i] = total[i] + o[i] * Dt;//位相
             }
             r[i][j] = theta[i];
         //位相差計算ーーーーーーーー
             pd1[i][j] = total[6] - total[i];

            //振動子のフラグ生成
            if(theta[i] >= 2*M_PI){//パルス発信判定
                theta[i] = theta[i] - 2 * M_PI;//位相
                o[i] = Omega;
                val[i] = 0.0;
                b.flag[i] = 1;
                b.time[i] = t;
                tb[i] += 1;
                
                //printf("%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t\n", o[6], o[7], o[8], o[11], o[12], o[13], o[16], o[17], o[18]);
                //パスロス計算
                for(k = 0; k < M; k++){//送信先

                    //レイリーフェージング
                    for(l = 0; l < FN; l++){
                        thetap[l]= genrand_real2() * 2 * M_PI;
                    }
                    //初期化
                    h.real = 0.0;
                    h.image = 0.0;

                    for (l = 0; l < FN; l++) {
                        h.real += cos(thetap[l]);
                        h.image += sin(thetap[l]);
                    }

                    h.real /= sqrt(FN);
                    h.image /= sqrt(FN);

                    ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
                    ss = 10 * log10(ss * ss);//フェージング係数

                    //判定

                    d[i][k] = sqrt( pow(ap[k].x - ap[i].x, 2) + pow(ap[k].y - ap[i].y, 2));

                    if(d[i][k]<=D){
                        d0 = d[i][k];
                    }else{
                        d0 = D0;
                    }

                    pl[i][k] = Ptx + 20*log10( (C/F) / (4.0*M_PI*d0) ) - 10 * n * log10(d[i][k]/d0);
                    pl[i][k] = pl[i][k] + ss + s[i][k];//パスロス

                //パルス受信判定および角速度l計算
                        if(b.time[k] > 0.0 && pl[i][k] > Pmin && pl[i][k] != 0.0 && i != k){ //
                            
                            w[k][i] = w[k][i] * pow( (1 - A), btime[i][k] / ( ( 2 * M_PI ) / Omega )) + A;//指数平滑移動平均
                                //fe = E * ( 1 - (pl[i][k] / Pmin) );//線形
                            
                                fe = E * ( 1 / sqrt( fabs(Pmax - pl[i][k]) ) );//2次関数
                            
                                //fe = E;//なし
                            
                            o[k] = o[k] + ( fe * w[k][i] * ( 1 - ( fabs(Omega * ( t - b.time[k])) /  M_PI  ) ) * sin( Omega * ( t - b.time[k]) ) );
                            
                            //val[k] += fe * w[k][i] * ( 1 - ( fabs(Omega * ( t - b.time[k])) /  M_PI  ) ) * sin( Omega * ( t - b.time[k]) );
                            

                            
                            if(o[k] < OmegaMin){
                                o[k] = OmegaMin;
                            }
                            
                            pk[i][k] += 1.0;
                            
                            btime[i][k] = t;//iから受けたビーコン受信タイミングの更新
                            
                            //printf("time;%8lf\t%d\n", t, i+1);
                            // printf("%d\t%8lf\n", b.flag[k], pl[i][k]);
                        }
                    //  plt[i][k] = pl[i][k];
//                    if(j%1000  == 0){
//                        printf("%8lf\t", o[k]);//パスロス
//                    }
                }//loop 送信先

                

            }//end loop パルス発信判定
            for(k = 0;k<M;k++){
                b.flag[k] = 0;
            }
            }//end loop送信側のAp番号
            //printf("%d;%d\t%8lf\t\n", i+1, b.flag[i], b.time[i]);
//        if(j%100  == 0){
//            printf("%8lf\n", o[6]);//パスロス
//        }
        }//end loop メインループ
        //printf("\n");

        
//位相差表示

//    for(j = 0;j<N;j+=1000){
//        printf("%8lf\t", ( (double)j + (double)m * N ) * Dt);
//            printf("%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t\t\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf", r[6][j], r[7][j], r[8][j], r[11][j], r[12][j], r[13][j], r[16][j], r[17][j], r[18][j], ( (double)j + (double)m * N ) * Dt, pd1[6][j], pd1[7][j], pd1[8][j], pd1[11][j], pd1[12][j], pd1[13][j], pd1[16][j], pd1[17][j], pd1[18][j]);
//           // printf("%8lf\t", r[i][j]);
//            //printf("%8lf\t", pd1[i][j]);
//        printf("\n");
//    }
        
//        for(j = 0;j<N;j+=1000){
//            printf("%8lf\t", ( (double)j + (double)m * N ) * Dt);
//                 printf("%8lf\t", r[i][j]);
//                printf("%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t%8lf\t\n", pd1[6][j], pd1[7][j], pd1[8][j], pd1[11][j], pd1[12][j], pd1[13][j], pd1[16][j], pd1[17][j], pd1[18][j]);
//        }


}//ROOP終わり
   //printf("\n");
    
//    for(i = -80;i<-50; i++){
//    fe = E * ( 1.0 - ( i*1.0 / Pmin ) );//変動引き込み係数計算
//        printf("%d\t%8lf\n", i, fe);
//    }
    //位相表示　複素次元
    for(i = 0; i < M; i++){
        apph[i].x = cos(r[i][N-1]);
        apph[i].y = sin(r[i][N-1]);
        //printf("%8lf\t%8lf\n", apph[i].x, apph[i].y);
        }

    
    //kmeans 初期中心点
    
    for(m = 0;m < ROOP;m++){
        for(j = 0;j < K;j++){
            ave[j].x = 0.0;
            ave[j].y = 0.0;
            gnum[j] = 0.0;
            disp[j].x = 0.0;
            disp[j].y = 0.0;
        }
        result = 0;
        mindisp = 0.0;
        
    cph[0] = 2 * M_PI * genrand_res53();//1つ目の中心点
    centers[0].x = cos(cph[0]);
    centers[0].y = sin(cph[0]);
    
    for(j = 1;j < K;j++){
        cph[j] = cph[j-1] + ( ( 2 * M_PI ) / K );
        centers[j].x = cos(cph[j]);
        centers[j].y = sin(cph[j]);
    }
    
    kmeans(apph, centers);
        for(i = 0;i < M;i++){
            group[i][m] = apph[i].g;
        }
        
        for(j = 0;j < K;j++){
            for(i = 0;i < M;i++){
                if(apph[i].g == j){
                    ave[j].x += apph[i].x;
                    ave[j].y += apph[i].y;
                    gnum[j] += 1.0;
                    }
            }
            ave[j].x = ave[j].x / gnum[j];
            ave[j].y = ave[j].y / gnum[j];
            //printf("\nave;%d\t%8lf\t%8lf\n\n", j, ave[j].x, ave[j].y);

            for(i = 0;i < M;i++){
                if(apph[i].g == j){
                    disp[j].x += pow( apph[i].x - ave[j].x, 2);
                    disp[j].y += pow( apph[i].y - ave[j].y, 2);
                }
            }
            disp[j].x = disp[j].x / gnum[j];
            disp[j].y = disp[j].y / gnum[j];
            //printf("\ndisp;%d\t%8lf\t%8lf\n\n", j, disp[j].x, disp[j].y);
            avedisp[m] += disp[j].x;
            avedisp[m] += disp[j].y;
        }
        avedisp[m] = avedisp[m] / (2 * K);
        //printf("\n%8lf\n", avedisp[m]);
    }
    
    for(m = 0;m < ROOP;m++){
        if(mindisp == 0.0 || mindisp > avedisp[m]){
            mindisp = avedisp[m];
            result = m;
        }
    }
    
    for(i = 0;i < M;i++){
        apph[i].g = group[i][result];
        //printf("%8lf\t%8lf\t%d\n", apph[i].x, apph[i].y, apph[i].g);
    }
 
    //グループ表示
    for(j = 0;j < K;j++){
        for(i = 0;i < M;i++){
            if(apph[i].g == j){
                //printf("%8lf\t%8lf\t%d\n", apph[i].x, apph[i].y, apph[i].g);
            }
        }
        //printf("\n");
    }

    //kmeans終わり
    
    //チャネルごとの利用電力
    for(j = 0;j < K;j++){//チャネルシフト
        for(i = 0;i < M;i++){
            if(apph[i].g == j){
                for(k = 0;k < M;k++){
                    if(apph[k].g == j && i != k){//同じチャネルであるか
                        for(l = 0; l < FN; l++){
                            thetap[l]= genrand_real2() * 2 * M_PI;
                            }
                            //初期化
                            h.real = 0.0;
                            h.image = 0.0;

                            for (l = 0; l < FN; l++) {
                                h.real += cos(thetap[l]);
                                h.image += sin(thetap[l]);
                            }

                            h.real /= sqrt(FN);
                            h.image /= sqrt(FN);

                            ss = ALPHA * sqrt(h.real * h.real + h.image * h.image);
                            ss = 10 * log10(ss * ss);//フェージング係数

                            //判定
                            if(d[i][k]<=D){
                                d0 = d[i][k];
                            }else{
                                d0 = D0;
                            }

                            pl[i][k] = Ptx + 20*log10( (C/F) / (4.0*M_PI*d0) ) - 10 * n * log10(d[i][k]/d0);
                            pl[i][k] = pl[i][k] + ss + s[i][k];//パスロス
                        //printf("%8lf\n", pl[i][k]);
                        gpl[i][re] += pow( 10.0, (pl[i][k] / 10.0) );
                        }//endif
                    }
                totalgpl[j][re] += gpl[i][re];//watt
                gpl[i][re] = 10.0*log10(gpl[i][re]);//dbm変換
                chpl[j][re] += gpl[i][re];
                gnum2[j][re] += 1.0;
                }//endif
            //printf("%d\t%2.10lf\n", i, gpl[i]);
            }
        totalgpl[j][re] = 10.0*log10(totalgpl[j][re]);//dbm変換
        avetotalgpl[j] += totalgpl[j][re];
        chpl[j][re] = chpl[j][re] / gnum2[j][re];//個々の平均
        avechpl[j] += chpl[j][re];
        printf("ch\t%d\tavePassloss\t%8lf\ttotalPassloss\t%8lf\n", j, chpl[j][re], totalgpl[j][re]);
        }
    
        
    }//総合繰り返しループ
    
    //平均算出
    for(j = 0;j < K;j++){//チャネルシフト
        avetotalgpl[j] =  avetotalgpl[j] / (double)TLOOP;
        avechpl[j] = avechpl[j] / (double)TLOOP;
        for(re = 0;re < TLOOP;re++){
            chdisp[j] += pow( chpl[j][re] - avechpl[j], 2);
            totalgdisp[j] += pow( totalgpl[j][re] - avetotalgpl[j], 2);
            }
        chdisp[j] = chdisp[j] / (double)TLOOP;
        totalgdisp[j] = totalgdisp[j] / (double)TLOOP;
        
        printf("ch\t%d\tavePassloss\t%8lf\ttotalPassloss\t%8lf\tPasslossdisp\t%8lf\tTotaldisp\t%8lf\t\n", j, avechpl[j], avetotalgpl[j], chdisp[j], totalgdisp[j]);
    }
    

}

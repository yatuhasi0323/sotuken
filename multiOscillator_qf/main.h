#ifndef __main
#define __main

//送信機の構造体
typedef struct {
    double x;
    double y;
    int g;
}point;

//２次元配列の構造体
typedef struct {
    double square[N][N];
}matrix;

typedef struct {
    double time[M];
    int flag[M];
}beacon;


typedef struct {
    double real;
    double image;
}rayleigh;

//空間周波数
typedef struct {
    double x[WN];
    double y[WN];
    double u[WN];
    double v[WN];
}frequency;

void init(double *m);
void kmeans(point *pap, point *centers);






#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <complex.h>

#include "parame.h"
#include "main.h"

int main(){
    int i;
    
    double pl = 0.0;
    double fe = 0.0;
    
    for(i = 0;i < N;i++){
        pl = (double)i * ( ( (double)Pmax - (double)Pmin) / (double)N) + (double)Pmin;
        fe = 1 / (1 + pow( M_E, (-SC * (pl -( (double)Pmax - ((double)Pmax - (double)Pmin)/2.0)) ) ) );
        printf("%lf\t%d\t%8lf\n", pl, i, fe);
    }
}
